package main

import (
	"fmt"
	"memcached/server"
	"os"
	"strconv"
)

var (
	DefaultHost = "127.0.0.1"
	DefaultPort = "9099"
)

func main() {
	var address = parseAddress(os.Args)
	s := server.New()
	s.Serve(address)
}

func parseAddress(args []string) string {
	if len(args) >= 3 {
		if v, err := strconv.Atoi(args[2]); err == nil {
			return fmt.Sprintf("%s:%d", DefaultHost, v)
		}
	}

	return fmt.Sprintf("%s:%s", DefaultHost, DefaultPort)
}
