package server

import (
	"errors"
	"sync"
)

type DataError error

var ErrItemNotFound DataError = errors.New("item not found")

type Record struct {
	Key        string
	Flags      int
	ExpTime    int
	BytesCount int
	Value      string
}

type RecordUpdater func(Record) (Record, error)

type dataStore struct {
	sync.RWMutex
	m map[string]Record
}

func NewDataStore() *dataStore {
	return &dataStore{
		m: map[string]Record{},
	}
}

func (d *dataStore) Get(k string) (Record, DataError) {
	d.RLock()
	defer d.RUnlock()

	if r, ok := d.m[k]; ok {
		return r, nil
	}

	return Record{}, ErrItemNotFound
}

func (d *dataStore) Set(k string, r Record) {
	d.Lock()
	defer d.Unlock()
	d.m[k] = r
}

func (d *dataStore) Update(k string, fn RecordUpdater) error {
	d.Lock()
	defer d.Unlock()

	if r, ok := d.m[k]; !ok {
		return ErrItemNotFound
	} else {
		updated, err := fn(r)
		if err != nil {
			return err
		}

		d.m[k] = updated
		return nil
	}
}

func (d *dataStore) Remove(k string) error {
	if _, ok := d.m[k]; !ok {
		return ErrItemNotFound
	}

	delete(d.m, k)
	return nil
}
