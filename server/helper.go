package server

import (
	"net"
	"strings"
)

func isConnectionTermination(s string) bool {
	return s == "^]"
}

func parseCommand(line string) Command {
	if line == "" {
		return CmdUnknown
	}

	var length int
	if len(line) >= 6 {
		length = 6
	} else {
		length = len(line)
	}

	sub := line[0:length]

	switch {
	case strings.HasPrefix(sub, "set"):
		return CmdSet
	case strings.HasPrefix(sub, "get"):
		return CmdGet
	case strings.HasPrefix(sub, "append"):
		return CmdAppend
	default:
		return CmdUnknown
	}
}

func readAllFromInput(conn net.Conn) ([]byte, error) {
	var data []byte
	n := 1
	startIdx := 0
	bufferLength := 1024
	for {
		if n == 1 {
			data = make([]byte, 1024*n)
		} else {
			tmpData := make([]byte, 1024*n)
			copy(tmpData, data)
			data = tmpData
		}

		buffer := data[startIdx:]
		_, err := conn.Read(buffer)
		if err != nil {
			return nil, err
		}

		if buffer[bufferLength-1] == '\x00' {
			return data, nil
		}

		n = n + 1
		startIdx = startIdx + 1
	}
}
