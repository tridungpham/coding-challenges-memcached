package server

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"

	"golang.org/x/exp/slog"
)

type Command int

const (
	CmdSet Command = iota
	CmdGet
	CmdAdd
	CmdAppend
	CmdUnknown
)

var (
	Protocol = "tcp"
	logger   = slog.New(slog.NewTextHandler(os.Stdout, nil))
)

type server struct {
	sync.Mutex
	waitingForData map[net.Conn]Record
	// dataStore      map[string]Record
	dataStore *dataStore
}

func New() *server {
	return &server{
		waitingForData: map[net.Conn]Record{},
		dataStore:      NewDataStore(),
	}
}

func (s *server) Serve(address string) {
	l, err := net.Listen(Protocol, address)
	if err != nil {
		logger.Error("error when listen :", err)
		os.Exit(1)
	}

	defer l.Close()
	logger.Info("Server listening", "address", address)

	count := 0

	for {
		conn, err := l.Accept()
		if err != nil {
			logger.Error("Error when accept connection", err)
			os.Exit(1)
		}

		logger.Info("New connection accepted")
		count = count + 1
		go s.handleConnection(conn, count)
	}
}

func (s *server) handleConnection(conn net.Conn, counter int) {
	var terminatedFromClient bool
	for {
		line, err := s.readLine(conn)
		if err != nil {
			logger.Error("error occurred when reading from connection", "err", err)
			break
		}

		if isConnectionTermination(line) {
			terminatedFromClient = true
			break
		}

		logger.Info("Handle input:", "input", line)

		if err = s.processLine(conn, line); err != nil {
			logger.Error("error when processing line", err)
		}
	}

	if err := conn.Close(); err != nil {
		logger.Error("Error when closing connection", err)
	} else {
		if terminatedFromClient {
			logger.Info("Connection closed by client")
		} else {
			logger.Info("Connection closed")
		}

	}
}

func (s *server) readLine(conn net.Conn) (string, error) {
	data, err := readAllFromInput(conn)
	if err != nil {
		return "", err
	}

	var cutSet = "\r\n\x00"
	if s.isWaitingForData(conn) {
		expectedLength := s.waitingForData[conn].BytesCount
		return strings.TrimRight(string(data[0:expectedLength]), cutSet), nil
	}

	return strings.TrimRight(string(data), cutSet), nil
}

func (s *server) processLine(conn net.Conn, line string) error {
	if s.isWaitingForData(conn) {
		return s.handleDataInput(conn, line)
	}

	switch parseCommand(line) {
	case CmdSet:
		return s.handleSet(conn, line)

	case CmdGet:
		return s.handleGet(conn, line)

	case CmdUnknown:
		return s.handleUnknownInput(conn, line)
	}

	return nil
}

func (s *server) handleSet(conn net.Conn, line string) error {
	parts := strings.Split(line, " ")

	// TODO For now we ignore validation for command options
	flags, _ := strconv.Atoi(parts[2])
	expTine, _ := strconv.Atoi(parts[3])
	bytesCount, _ := strconv.Atoi(parts[4])

	record := Record{
		Key:        parts[1],
		Flags:      flags,
		ExpTime:    expTine,
		BytesCount: bytesCount,
	}

	s.waitingForData[conn] = record
	return nil
}

func (s *server) handleGet(conn net.Conn, line string) error {
	parts := strings.Split(line, " ")

	keyName := parts[1]
	if r, err := s.dataStore.Get(keyName); err != nil {
		return s.reply(conn, "END")
	} else {
		answer := fmt.Sprintf("VALUE %s %d %d", r.Value, r.Flags, r.BytesCount)
		return s.reply(conn, answer)
	}
}

func (s *server) handleDataInput(conn net.Conn, line string) error {
	s.Lock()
	item := s.waitingForData[conn]
	item.Value = line
	s.dataStore.Set(item.Key, item)
	delete(s.waitingForData, conn)
	s.Unlock()
	return s.reply(conn, "STORED")
}

func (s *server) handleUnknownInput(conn net.Conn, line string) error {
	return s.reply(conn, "UNKNOWN COMMAND")
}

func (s *server) reply(conn net.Conn, output string) error {
	_, err := conn.Write([]byte(output + "\r\n"))
	return err
}

func (s *server) isWaitingForData(conn net.Conn) bool {
	_, ok := s.waitingForData[conn]
	return ok
}
